# Warnings
Flags: -Wall -Wextra -Wduplicated-cond -Wduplicated-branches -Wlogical-op -Wrestrict -Wnull-dereference -Wjump-misses-init -Wshadow
## 1. -Wduplicated-cond
Warn about duplicated conditions in if-elseif chains
## 2. -Wduplicated-branches
Warn when an if-else has identical branche
## 3. -Wlogical-op
Warn about use of logical operations where a bitwise operation probably was intended
## 4. -Wrestrict
Warn when the compiler detects that an argument passed to a restrict or __restrict qualified parameter alias with another parameter
## 5. -Wnull-dereference
Warn when the compiler detects paths that dereferences a null pointer
## 6. -Wjump-misses-init
Warn if a goto statement or a switch statement jumps forward across the initialization of a variable, or jumps backward to a label after the variable has been initialized.

```C

int foo(int a)
{
  int b;
  switch (a)
  {
  case 0:
    b = 0;
    int c = 42;
    break;
  default:
    b = c;  // c not initialized here
  }
  return b;
}

```

## 7. -Wshadow
Warn when a local variable or type declaration shadows another variable, parameter, type, or class member

# List of warnings that should be treated as errors (* not enabled by Wall Wextra Wpedantic)
* -Wuninitialized 
* -Winit-self* (can only be used with -Wuninitialized
* -Wimplicit
* -Wimplicin-int
* -Wreturn-type
* -Wmemset-transposed-args
* -Wmemset-el-size
* -Wsizeof-array-argument
* -Wsizeof-pointer-memaccess
* -Wsizeof-pointer-div
* -Wjump-misses-init*  
* -Wmaybe-uninitialized
* -Wmemset-transposed-args
* -Wbool-compare
* -Wconversion*
* -Wduplicated-cond*
* -Wempty-body
* -Wenum-compare
* -Wincompatible-pointer-types
* -Wint-conversion
* -Wint-to-pointer-cast (to reconsider)
* -Wlogical-op*
* -Wmissing-prototypes* (to reconsider)
* -Wmultichar*
* -Wnested-externs*
* -Wparentheses (to reconsider)
* -Wpointer-arith *
* -Wredundant-decls * (threating as error is to reconsider, worth to enable at least as a warning)
* -Wreturn-type
* -Wsequence-point
* -Wshift-count-negative
* -Wshift-count-overflow
* -Wshift-negative-value
* -Wswitch-bool
* -Wtrigraphs

